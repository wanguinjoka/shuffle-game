import React from 'react';
import './SingleCard.css'

export default function SingleCard( {card, handleChoice , flipped, disabled} ) {
    const handleClick =() =>  {
      if(!disabled){
        handleChoice(card)
      }
    }
    return (
        <div>
            <div className='card' >
              <div className = {flipped ? "flipped": ""}>
                <img className="front" src={card.src} alt="front-card"/>
                <img className="back" src="/img/cover.png" alt="cover-card" onClick={handleClick} />
              </div>
            </div>
        </div>
    )
}
