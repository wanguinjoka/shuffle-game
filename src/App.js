
import './App.css';
import {useState, useEffect} from 'react';
import SingleCard from './components/SingleCard';

const cardImages = [
  { "src": "/img/helmet-1.png", matched:false},
  { "src": "/img/potion-1.png",matched:false},
  { "src": "/img/ring-1.png", matched:false},
  { "src": "/img/scroll-1.png",matched:false },
  { "src": "/img/shield-1.png",matched:false },
  { "src": "/img/sword-1.png", matched:false }

]

function App() {

  const [cards, setCards] = useState([])
  const [turns, setTurns] = useState(0)
  const [choiceOne, setchoiceOne] = useState(null)
  const [choiceTwo, setChoiceTwo] = useState(null) 
  const [disabled, setDisabled] = useState(false)                                                                   

  // func that is going to dupliccate the cards/imgs, sort and add property of index to new array 
  const shuffleCards = () => {
    const shuffleCards = [...cardImages, ...cardImages]
    .sort(() => Math.random() - 0.5) 
    .map((card) =>({ ...card, id: Math.random() }))
    
    setchoiceOne(null)
    setChoiceTwo(null)
    setCards(shuffleCards)
    setTurns(0)
  }

  // handle func of choice
 const handleChoice = (card) => {
   choiceOne ? setChoiceTwo(card) : setchoiceOne(card)
 }
 // compare two cards
 useEffect(() => {
    if(choiceOne && choiceTwo){
      setDisabled(true)
      if(choiceOne.src === choiceTwo.src){
        setCards(prevCards => {
          return prevCards.map(card =>{
            if(card.src === choiceTwo.src){
              return {...card, matched:true}
            }else{
              return card
            }
          })
        })
        resetTurn()
      }else{
        setTimeout(() => resetTurn() , 1000)
      }
    }
 }, [choiceOne, choiceTwo])

 console.log(cards)

 //reset choices & increase turn
 const resetTurn = () => {
   setchoiceOne(null)
   setChoiceTwo(null)
   setTurns(prevTurns => prevTurns + 1)
   setDisabled(false)
 }

 useEffect(()=>{
   shuffleCards()
 }
 ,[])

  return (
    <div className="App">
      <h1>Magic Match</h1>
        <button onClick={() => {shuffleCards()}}>New Game</button>
        <div className="card-grid">
           {cards.map(card => ( 
            // <div key={card.id}>
            //   <div>
            //     <img className="front" src={card.src} alt="front-card"/>
            //     <img className="back" src="/img/cover.png" alt="cover-card" />
            //   </div>
            // </div> */}
          < SingleCard 
                     key={card.id}
                    //  this are props
                     card={card} 
                     handleChoice={handleChoice}
                     flipped={card === choiceOne || card === choiceTwo || card.matched}
                     disable={disabled}/>
          ))}
        </div>
        <p> Turns:{turns}</p>

    </div>
  );
}

export default App;
